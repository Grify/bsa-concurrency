package bsa.java.concurrency.fs;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class FileSystemService implements FileSystem{

    @Async
    public CompletableFuture<Void> saveFile(String path, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
            if (file.length > 0) {
                try (FileOutputStream fos = new FileOutputStream(path)) {
                    fos.write(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });
    }

    public CompletableFuture<byte[]> getFileByName(String fileName) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readAllBytes(new File("src/main/resources/files/" + fileName).toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });

    }

    @Async
    public CompletableFuture<Void> deleteByName(String fileName) {
        return CompletableFuture.supplyAsync(() -> {
            String pathToFiles = "src/main/resources/files";
            File dir = new File(pathToFiles);

            for (File file : Objects.requireNonNull(dir.listFiles())) {
                if (file.getName().equals(fileName)) {
                    file.delete();
                    break;
                }
            }
            return null;
        });

    }

    @Async
    public CompletableFuture<Void> purgeAll() {
        return CompletableFuture.supplyAsync(() -> {
            String pathToFiles = "src/main/resources/files";
            File dir = new File(pathToFiles);

            for (File file : Objects.requireNonNull(dir.listFiles())) {
                file.delete();
            }
            return null;
        });

    }

}
