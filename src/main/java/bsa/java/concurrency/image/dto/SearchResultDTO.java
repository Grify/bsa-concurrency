package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class SearchResultDTO {
    private UUID id;
    private Double match;
    private String image;
}
