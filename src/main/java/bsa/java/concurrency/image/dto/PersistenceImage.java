package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class PersistenceImage {
    private UUID imageId;
    private long hash;
    private String imageName;
}
