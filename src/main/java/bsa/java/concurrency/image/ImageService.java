package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.image.dto.PersistenceImage;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class ImageService {

    private List<PersistenceImage> files = new ArrayList<>();

    @Autowired
    FileSystemService fileSystemService;

    @Async
    public CompletableFuture<Void> batchFile(byte[] file, String fileName) {
        return CompletableFuture.supplyAsync(() -> {
            UUID id = UUID.randomUUID();
            String postfix = "." + fileName.split("\\.")[1];
            String imageUrl = "src/main/resources/files/" + id + postfix;
            CompletableFuture<Void> saveFileResponse = fileSystemService.saveFile(imageUrl, file);
            long hash = calculateHash(file);
            try {
                saveFileResponse.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            PersistenceImage image = new PersistenceImage(id, hash, id + postfix);
            files.add(image);

            return null;
        });

    }

    public List<SearchResultDTO> search(MultipartFile file, double threshold) {
        if (threshold > 1 || threshold <= 0) {
            return null;
        }
        String pathToImages = "http://127.0.0.1:8080/image/files/";
        List<SearchResultDTO> response = new ArrayList<>();
        try {
            long hash = calculateHash(file.getBytes());
            for (PersistenceImage item : files) {
                double matchPercent = 1 - compareHashes(hash, item.getHash());
                if (threshold <= matchPercent) {
                    response.add(new SearchResultDTO(item.getImageId(), matchPercent * 100, pathToImages + item.getImageName()));
                }
            }
            if (response.size() == 0) {
                batchFile(file.getBytes(), file.getOriginalFilename());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void deleteById(UUID id) {
        String fileName = "";
        for (PersistenceImage file : files) {
            if (file.getImageId().equals(id)) {
                fileName = file.getImageName();
                files.remove(file);
                break;
            }
        }
        if (!fileName.equals("")) {
            fileSystemService.deleteByName(fileName);
        }
    }

    public void purgeAll() {
        files.clear();
        fileSystemService.purgeAll();
    }

    @PostConstruct
    public void uploadFilesToCache() {
        String pathToFiles = "src/main/resources/files";
        File dir = new File(pathToFiles);
        if (! dir.exists()) {
            new File(pathToFiles).mkdir();
            return;
        }
        for (File file : Objects.requireNonNull(dir.listFiles())) {
            try {
                String fileName = file.getName();
                UUID id = UUID.fromString(fileName.split("\\.")[0]);
                long hash = calculateHash(Files.readAllBytes(file.toPath()));
                files.add(new PersistenceImage(id, hash, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public double compareHashes(long hash1, long hash2) {
        long xor = hash1 ^ hash2;
        int ct = 0;
        while (xor != 0) {
            xor = xor & (xor - 1);
            ct++;
        }
        return (double) ct / 64;
    }

    @Async
    public long calculateHash(byte[] image) {
        try {
            var img = ImageIO.read(new ByteArrayInputStream(image));
            return calculateDHash(preprocessImage(img));
        } catch (Exception err) {
            throw new RuntimeException(err.getMessage());
        }
    }

    private static BufferedImage preprocessImage(BufferedImage image) {
        var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var output = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);

        return output;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (var row = 1; row < 9; row++) {
            for (var col = 1; col < 9; col++) {
                var prev = brightnessScore(processedImage.getRGB(col - 1, row - 1));
                var current = brightnessScore(processedImage.getRGB(col, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }

        return hash;
    }
}
