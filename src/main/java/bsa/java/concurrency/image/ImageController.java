package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    ImageService imageService;

    @Autowired
    FileSystemService fileSystemService;

    Logger logger = LoggerFactory.getLogger(ImageController.class);

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        logger.info("Path: /batch. Method: POST. Size: \"" + files.length + "\"");
        try {
            List<CompletableFuture<Void>> futures = new ArrayList<>();
            for(MultipartFile file : files) {
                futures.add(imageService.batchFile(file.getBytes(), file.getOriginalFilename()));
            }
            for (CompletableFuture<Void> future : futures) {
                future.get();
            }
        } catch (IOException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        logger.info("Path: /search. Method: POST. File name: \"" + file.getOriginalFilename() + "\"");
        return imageService.search(file, threshold);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        logger.info("Path: /{id}. Method: DELETE. ID: \"" + imageId.toString() + "\"");
        imageService.deleteById(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        logger.info("Path: /purge. Method: DELETE.");
        imageService.purgeAll();
    }

    @GetMapping(value = "/files/{fileName}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getFile(@PathVariable String fileName) throws ExecutionException, InterruptedException {
        logger.info("Path: /files/{fileName}. Method: GET. File name: \"" + fileName + "\"");
        return fileSystemService.getFileByName(fileName).get();
    }
}
